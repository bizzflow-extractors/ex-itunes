FROM node:16.11.0-alpine

WORKDIR /code

ADD . .

RUN npm install

ENTRYPOINT node /code/run.js