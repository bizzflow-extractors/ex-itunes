# iTunes Reporter Extractor v3

## Description

Integration of iTunes  Reporter API with [Bizzflow](https://www.bizzflow.net/) and [Keboola Connection](https://connection.keboola.com/). Written in Node.js & [Apple iTunes Reporter API](https://help.apple.com/itc/contentreporterguide/#/itc0f2481229).

### What it is used for?

Current version supports downloading both **Sales data** and **Financial (Earnings)**. **Sales data** can be downloaded with **Daily/Summary** settings per **VendorId**. **Earnings** on the other hand can be downloaded with **5-4-4 fiscal dimension settings**. Per VendorId as well.



## How to use it?

    {
      "account": "iTunes account number",
      "accessToken": "iTunes accessToken",
      "vendors": [ vendorId1, ..., vendorIdN ],
      "reportType": "sales|financial",
      "startDate": "YYYYMMDD (optional, if not specified, a default date is used)",
      "endDate": "YYYYMMDD (optional, if not specified, a default date is used)",
    }

Note: Default date is today - 2. If you don't specify optional parameters ("startDate", "endDate"), it will be applied. Otherwise the range from both attributes ("startDate", "endDate") will be used (the date format is YYYYMMDD).


### How to get accessToken and account number
For both you will need [Apple reporter](https://help.apple.com/itc/contentreporterguide/#/apda86f89da5) so download it.

You account number can be found following this [guide](https://help.apple.com/itc/contentreporterguide/#/itcccef1d795)
You can view or generate you access token following this [guide](https://help.apple.com/itc/contentreporterguide/#/apd2f1f1cfa3)

!!! Important you access token expire after 180 days, so you need to change it regularly.   


### Local use

1) Create or modify the config.json file:

2) Run the run.js
      ```sh
      node run.js <path-to-config-file> <output-folder-path>
      ```